#!/bin/bash

# Common

code --install-extension wallabyjs.quokka-vscode
code --install-extension wallabyjs.wallaby-vscode
code --install-extension jebbs.plantuml
code --install-extension visualstudioexptteam.vscodeintellicode
code --install-extension humao.rest-client
code --install-extension DrMerfy.overtype

# Themes, colorizing

code --install-extension zamerick.black-ocean
# code --install-extension coenraads.bracket-pair-colorizer -> Use "editor.bracketPairColorization.enabled": true in settings

# Styling, Linting, Formatting

code --install-extension hookyqr.beautify
code --install-extension dbaeumer.vscode-eslint
code --install-extension editorConfig.editorConfig
code --install-extension esbenp.prettier-vscode
code --install-extension shinnn.stylelint

# Source Control

code --install-extension eamodio.gitlens
code --install-extension donjayamanne.githistory
code --install-extension github.vscode-pull-request-github

# Angular

# code --install-extension angular.ng-template
# code --install-extension infinity1207.angular2-switcher
# code --install-extension formulahendry.auto-close-tag

# DevOps

# code --install-extension ms-azuretools.vscode-docker
# code --install-extension mauve.terraform

# Optional

# code --install-extension ryanluker.vscode-coverage-gutters
# code --install-extension devzstudio.emoji-snippets
# code --install-extension rbbit.typescript-hero
# code --install-extension softwaredotcom.swdc-vscode  # code time

# Tools

# code --install-extension rebornix.toggle
# code --install-extension richie5um2.vscode-statusbar-json-path
# code --install-extension funkyremi.vscode-google-translate
# code --install-extension simonsiefke.svg-preview
# code --install-extension mohsen1.prettify-json
