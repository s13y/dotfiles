"use strict";

// Contient seulement les valeurs différentes de l'original

module.exports = {
    config: {
	    opacity: 1, // plugin

        fontSize: 14,
        fontFamily: '"JetBrainsMono NF", Menlo, "DejaVu Sans Mono", Consolas, "Lucida Console", monospace',

        cursorColor: 'rgba(255, 255, 0, 0.8)',
        cursorBlink: true,
        borderColor: '#777',

        workingDirectory: '',
        shell: 'C:\\Windows\\System32\\wsl.exe',
        shellArgs: [],

        disableLigatures: false,
        preserveCWD: false,
    },
    plugins: ["hyper-opacity", "hyper-pane", "hyper-highlight-active-pane"],

    keymaps: {
        "editor:copy": "ctrl+c",
        "editor:paste": "ctrl+v",
        "pane:next": "ctrl+pagedown",
        "pane:prev": "ctrl+pageup",
    },
};
//# sourceMappingURL=config-default.js.map