#!/bin/bash

echo "Outdated"

# Installer nvm

sudo apt-get install build-essential libssl-dev -y

# Installer la dernière LTS de node

echo "Check latest version of nvm at https://github.com/creationix/nvm/releases."

read

curl https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash

echo "source ~/.zshrc"
echo "nvm install --lts"

echo "On macOS:"
echo "  $ ln -s $(which node) /usr/local/bin/node"
echo "  $ ln -s $(which npm) /usr/local/bin/npm"
