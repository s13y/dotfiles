# VPS Ubuntu Server

    $ bash <(wget -qO- https://gitlab.com/s13y/dotfiles/raw/master/linux/init-vps.sh)

## Interactive setup

    $ bash <(wget -qO- https://gitlab.com/s13y/dotfiles/raw/master/linux/setup.sh)

## Powerlevel9k

Sur Mac, la font qui fonctionne est `Hack Regular Nerd Font Complete`.

Sur WSL `Meslo NF` avec WSL-Terminal.

# Windows

    Set-ExecutionPolicy Bypass; iex ((New-Object System.Net.WebClient).DownloadString('https://gitlab.com/s13y/dotfiles/raw/master/windows/setup-ps.ps1'))

    Set-ExecutionPolicy Bypass; iex ((New-Object System.Net.WebClient).DownloadString('https://gitlab.com/s13y/dotfiles/raw/master/windows/chocolatey.ps1'))

# Troubleshooting

Si certaines icons ne s'affichent pas, probablement qu'il manque le mode, ou il est placé au mauvais endroit:

    POWERLEVEL9K_MODE="nerdfont-complete"

S'il y a `prompt_load:43: command not found: bc` à chaque fois que le prompt s'affiche, il faut installer `bc` (`sudo apt-get install bc`).

# Sources

- [jldeen dotfiles repo](https://github.com/jldeen/dotfiles/blob/wsl/macos/set-defaults.sh) ([WSL Configure](https://github.com/jldeen/dotfiles/blob/wsl/configure.sh) and [set-defaults.sh](https://github.com/jldeen/dotfiles/blob/wsl/macos/set-defaults.sh))
- [mathiasbynens dotfiles repo](https://github.com/mathiasbynens/dotfiles/blob/master/.macos) (and [.macos](https://github.com/mathiasbynens/dotfiles/blob/master/.macos))
