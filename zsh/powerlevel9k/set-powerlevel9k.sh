#!/bin/bash

sed -i 's/ZSH_THEME="robbyrussell"/POWERLEVEL9K_MODE="nerdfont-complete"\nZSH_THEME="powerlevel9k\/powerlevel9k"/g' ~/.zshrc

cat powerlevel9k-settings >> ~/.zshrc

echo "Faire : source ~/.zshrc"
