# Pour Oh-My-ZSH, le fichier `aliases.zsh` doit être placé dans $ZSH_CUSTOM
# qui est généralement quelque chose comme /home/<user>/.oh-my-zsh/custom

# GIT

alias gmb='git merge-base develop $(current_branch)'
alias gmbr='git rebase -i $(gmb)'
alias grbd='git rebase develop'
alias grbid='git rebase -i develop'
alias grbids='git rebase --exec '\''git commit --amend --no-edit -n -S'\'' -i develop'

alias ggrh=git reset --hard origin/$(current_branch)

alias glr='git pull --rebase'
alias gpocbff='git push -u origin ($current_branch) --force'
alias ggpf='git push --force-with-lease origin $(current_branch)'

alias glcp='git --no-pager log --pretty="%h - %ar - %s [%an]" --no-merges'

alias gscmsg='git commit -S -m'

alias grbids='git rebase --exec '\''git commit --amend --no-edit -n -S'\'' -i development'

gfr() { 
    git reset @~ "$@" && git commit --amend --no-edit 
}

gsth() {
    git stash push -m "$1"
}

gstn() {
    git stash apply stash^{/$1}
}

gldel() {
    # get history of deleted file
    # use: gldel **/somefile
    git log --all --full-history --pretty="%h - %ar - %s [%an]" --no-merges -- $1
}

# DOCKER

alias dcls='docker container ls'
alias dclsf='docker container ls --format "table {{.Names}}\t{{.Status}}\t{{.Ports}}"'
alias dclsa='docker container ls --all'
alias dcs='docker container stop'
alias dcrm='docker container rm'
alias dr='docker run'
alias dia='docker images --all'
alias dirm='docker image rm'

# ADMIN

# alias bwu='export BW_SESSION="`bw unlock $1 --raw`"'
alias pubkey="more ~/.ssh/id_rsa.pub | pbcopy | echo '=> Public key copied to pasteboard.'"

# NETWORK

function myip() {
  ifconfig en0 | grep 'inet ' | sed -e 's/:/ /' | awk '{print "en0 (IPv4): " $2 " " $3 " " $4 " " $5 " " $6}'
}

# Init node project

function node-project {
  git init
  npx license $(npm get init.license) -o "$(npm get init.author.name)" > LICENSE
  npx gitignore node
  npx covgen "$(npm get init.author.email)"
  npm init -y
  git add -A
  git commit -m "chore: initial commit"
}

# Divers

alias meteo='curl wttr.in/Montreal'
