#!/bin/bash

sudo apt update -y
sudo apt upgrade -y

sudo apt install deluged deluge-web deluge-console -y

sudo setsid sh -c 'exec deluged <> /dev/tty2 >&0 2>&1'
sudo pkill deluged

deluge-console "config -s allow_remote True"

echo ""
echo "====[ ~/.config/deluge/auth modification ]=============================="
echo "User name for deluged:"
read user
echo "Password for deluged:"
read password
echo "$user:$password:10" >> ~/.config/deluge/auth

echo ""
echo "====[ Copy deluged{-web}.service ]============================================"
sudo cp ./deluged.service /etc/systemd/system
sudo cp ./deluge-web.service /etc/systemd/system

sudo systemctl enable deluged.service
sudo systemctl enable deluge-web.service
