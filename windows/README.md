# Windows Setup

Installe les applications et les outils via `winget`.

Si `winget` n'est pas présent, installer avec cette commande dans PowerShell:

```
Add-AppxPackage -RegisterByFamilyName -MainPackage Microsoft.DesktopAppInstaller_8wekyb3d8bbwe
```

## Les scripts

Liste des scripts:

*  `00-install-basic.ps1`: installe la plupart des applications de base.
*  `01-scoop-install.ps1`: installe scoop et les fonts Nerd Fonts
*  `02-office.ps1`: installe les applications spécifiques au travail.

## Quick setup

Avec une invite PowerShell classique:

```
iex ((New-Object System.Net.WebClient).DownloadString('https://gitlab.com/s13y/dotfiles/-/raw/master/windows/00-install-basic.ps1'))
```

## Autres setups

### Clé SSH

Les commandes suivantes prennent en compte le PowerShell 7+.

```
ssh-keygen -t ed25519 -a 256 [-C "user@host"]
cat ~\.ssh\id_ed25519.pub
```

### Git

```
git config --global user.name "<votre nom>"
git config --global user.email <votre_email>
```

### Installation de polices

Polices pour coder: installer les fonts via `scoop`, les Nerd Font fonctionnent bien avec le Terminal Windows et Ubuntu sur WSL2 + Oh my Zsh.

Polices couramment utilisés, disponibles sur Google Fonts:

* [Roboto](https://fonts.google.com/specimen/Roboto), [Roboto Condensed](https://fonts.google.com/specimen/Roboto+Condensed)
* [Open Sans](https://fonts.google.com/specimen/Open+Sans)
* [Lato](https://fonts.google.com/specimen/Lato)
* [Inter](https://fonts.google.com/specimen/Inter)
* [Fira Sans](https://fonts.google.com/specimen/Fira+Sans)

Autres fonts:

* [Roboto Slab](https://fonts.google.com/specimen/Roboto+Slab)
* [Barlow](https://fonts.google.com/specimen/Barlow), [Barlow Semi-Condensed](https://fonts.google.com/specimen/Barlow+Semi+Condensed), [Barlow Condensed](https://fonts.google.com/specimen/Barlow+Condensed)
* [IBM Plex Sans](https://fonts.google.com/specimen/IBM+Plex+Sans)


[Google Font share](https://fonts.google.com/share?selection.family=Barlow+Condensed:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900|Barlow+Semi+Condensed:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900|Fira+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900|Inter:wght@100..900|Lato:ital,wght@0,100;0,300;0,400;0,700;0,900;1,100;1,300;1,400;1,700;1,900|Open+Sans:ital,wght@0,300..800;1,300..800|Roboto+Condensed:ital,wght@0,100..900;1,100..900|Roboto+Slab:wght@100..900|Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900|Victor+Mono:ital,wght@0,100..700;1,100..700)
