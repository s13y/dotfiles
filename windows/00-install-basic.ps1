Write-Host "----------------------------------------------------" -ForegroundCotor Green
Write-Host "Windows Applications Install Script" -ForegroundCotor Green
Write-Host "----------------------------------------------------" -ForegroundCotor Green

$installNVM = (Read-Host "Install NVM for Windows? (otherwise will do NodeJS's install)").ToLower()
$installFastCopy = (Read-Host "Install FastCopy? (License of 5.x does not allow on workplaces)").ToLower()
$installTeams = (Read-Host "Install Teams?").ToLower()
$installDotNetTools = (Read-Host "Install Tools for .NET dev? (linqpad, .NET sdk)").ToLower()
$installVSCommunity = (Read-Host "Install Visual Studio Community and Fiddler Classic?").ToLower()
$installFiddlerClassic = (Read-Host "Install Fiddler Classic?").ToLower()
$installDocker = (Read-Host "Install Docker?").ToLower()
$installMultimediaApps = (Read-Host "Install media players (Spotify, VLC)?").ToLower()
$installGamingTools = (Read-Host "Install Gaming Tools (Steam, Epic, Discord)? [Y] Yes or [N] No ").ToLower()
$installDesignTools = (Read-Host "Install Inkscape, Figma and FontBase? [Y] Yes or [N] No ").ToLower()

Write-Host "----------------------------------------------------" -ForegroundCotor Green
Write-Host "Installing bare minimum apps..." -ForegroundCotor Green
Write-Host "----------------------------------------------------" -ForegroundCotor Green

if ($installNVM -eq "y") {
    winget install CoreyButler.NVMforWindows
} else {
    winget install OpenJS.NodeJS.LTS
}

winget install Git.Git
winget install CoreyButler.NVMforWindows
winget install Microsoft.WindowsTerminal
winget install Microsoft.PowerShell
winget install Microsoft.VisualStudioCode
winget install Bitwarden.Bitwarden
winget install Mozilla.Firefox
winget install 7zip.7zip
winget install JetBrains.Toolbox
winget install SublimeHQ.SublimeText.4
winget install PDFgear.PDFgear
winget install Gyan.FFmpeg
winget install jqlang.jq
winget install NickeManarin.ScreenToGif
winget install Chocolatey.Chocolatey
winget install dotPDN.PaintDotNet

winget install Microsoft.PowerToys
winget install Piriform.Speccy
winget install voidtools.Everything

if ($installTeams -eq "y") {
    winget install Microsoft.Teams
}

if ($installDotNetTools -eq "y") {
    winget install Microsoft.DotNet.SDK.8
    winget install LINQPad.LINQPad.8
}

if ($installVSCommunity -eq "y") {
    winget install Microsoft.VisualStudio.2022.Community
}

if ($installFiddlerClassic -eq "y") {
    winget install Telerik.Fiddler.Classic
    # Alternatives:
    # winget install HTTPToolKit.HTTPToolKit
}

if ($installDocker -eq "y") {
    winget install Docker.DockerDesktop
}

if ($installFastCopy -eq "y") {
    winget install FastCopy.FastCopy
}

Write-Host "----------------------------------------------------" -ForegroundCotor Green
Write-Host "Installing multimedia and game stores..." -ForegroundCotor Green
Write-Host "----------------------------------------------------" -ForegroundCotor Green

if ($installMultimediaApps -eq "y") {
    winget install Spotify.Spotify
    winget install VideoLAN.VLC
}

if ($installGamingTools -eq "y") {
    winget install Valve.Steam
    winget install EpicGames.EpicGamesLauncher
    winget install -e --id Discord.Discord
}

if ($installDesignTools -eq "y") {
    winget install Inkscape.Inkscape
    winget install Figma.Figma
    winget install Levitsky.FontBase
}
