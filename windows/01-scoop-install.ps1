# Execute with admin privileges
Set-ExecutionPolicy RemoteSigned

iwr -useb get.scoop.sh | iex

scoop bucket add nerd-fonts

scoop install Hack-NF
scoop install FiraCode-NF
scoop install JetBrainsMono-NF
scoop install VictorMono-NF
