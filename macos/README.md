# macOS Setup

- git
- ssh key
- brew

## Scripts

1. `git --version` -> Installe les outils de dev sur macOS.
2. `git clone https://gitlab.com/s13y/dotfiles.git`
3. `cd dotfiles/macos`
4. Exécuter les scripts dans l'ordre.

## Autres configurations

* [Git Setup](../docs/git-setup.md)
