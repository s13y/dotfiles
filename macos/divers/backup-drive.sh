#! /bin/bash

###############################################################################
# Le nom du backup
BACKUPNAME="Documents LACIE_200_2"

# Utilisation de Growl
GROWLHOST=localhost
GROWLPASS=password

# Nom des fichiers (ne pas modifier les extensions)
NAMET=/Volumes/MacOSX_Backup/Temp/documents-$(date +%Y%m%d).tar.gz
NAMEE=/Volumes/MacOSX_Backup/Temp/documents-$(date +%Y%m%d).aes

# Le répertoire/volume à sauvegarder
DIRBACKUP=/Volumes/LACIE_200_2/Divers2

# cette variable est le mot de passe utilisé pour l'encryption
ENCRYPTPASS=dontknow

# Cette variable dit au script si l'on veut effacer la copie de la sauvegarde située dans le profile
REMOVETEMPE=NO

#=== SAUVEGARDE FTP ==========================================================#
# Indiquez si oui (YES) ou non (NO), il faut utiliser le FTP
ENABLEFTP=NO
# Les infos pour la connexion FTP
FTPSITE=
FTPUSER=
FTPPASS=
# L'endroit pour télécharger la sauvegarde
FTPBACKUPDIR=/backups/documents.aes

#=== SAUVEGARDE SAMBA ========================================================#

#This variable tells the script if you want use a samba server to backup
ENABLESMB=NO
#These variables set the account details to use for samba backup
SMBSERVER=
SMBUSER=
#This variable sets the share name that you wish to backup to, use this format //ACCOUNT@SERVER/SHARENAME
SMBSHARE=
#This variable sets the mount location
SMBBACKUPDIR=/Volumes/backupdrive/

#=== SAUVEGARDE DISQUE DUR/EXTERNE ===========================================#

#This variable will tell the script if you want to backup to another hard drive or partition
ENABLEHDD=YES
#This variable sets the location of the mounted drive you wish to backup to
HDDMOUNT=/Volumes/MacOSX_Backup

#=== SAUVEGARDE iDisk =========================================================#

#This variable will tell the script if you want to backup to a iDisk
ENABLEIDISK=NO

################################################################################
#The backup script
#
#
#Do not modify below this line unless you know what you are doing
#
#
#################################################################################
#echo "Announcing Backup"
growlnotify -m "La sauvegarde de $BACKUPNAME a débuté..." -t "Sauvegarde" -s -P $GROWLPASS --udp --host $GROWLHOST

#Archives the files
#echo "archiving files"
growlnotify -m "Archivage de $BACKUPNAME..." -t "Sauvegarde" -P $GROWLPASS --udp --host $GROWLHOST
tar -czfv $NAMET $DIRBACKUP

#Encrypts the archive
#echo "encrypting files"
#growlnotify -m "Encrypting $BACKUPNAME" -t "Backup" -P $GROWLPASS --udp --host $GROWLHOST
#openssl enc -e -a -salt -aes-256-cbc -in $NAMET -out $NAMEE -k $ENCRYPTPASS

if [ "$ENABLEFTP" = "YES" ]; then
#uploads encrypted archive to the ftp
growlnotify -m "Uploading $BACKUPNAME to FTP" -t "Sauvegarde" -P $GROWLPASS --udp --host $GROWLHOST
echo "uploading to ftp"
ftp -n <<EOF
open $FTPSITE
user $FTPUSER $FTPPASS
put $NAMEE $FTPBACKUPDIR
bye
EOF
fi

if [ "$ENABLESMB" = "YES" ]; then
#mounts the backup drive
echo "Mounting backupdrive"
growlnotify -m "Mounting Backup Drive" -t "Sauvegarde" -P $GROWLPASS --udp --host $GROWLHOST
mount_smbfs -I $SMBSERVER -U $SMBUSER $SMBSHARE $SMBBACKUPDIR

#uploads encrypted archive to the server
echo "Uploading archive to server"
growlnotify -m "Copying to Backup Drive" -t "Sauvegarde" -P $GROWLPASS --udp --host $GROWLHOST
cp $NAMEE $SMBBACKUPDIR

#unmounts the backupdrive
echo "unmounting backupdrive"
growlnotify -m "Unmounting backupdrive" -t "Sauvegarde" -P $GROWLPASS --udp --host $GROWLHOST
umount $SMBBACKUPDIR
fi

if [ "$ENABLEHDD" = "YES" ]; then
#copies encrypted archive to the drive
echo "Copying archive to drive "
growlnotify -m "Copying to Backup Drive" -t "Sauvegarde" -P $GROWLPASS --udp --host $GROWLHOST
cp $NAMEE $HDDMOUNT
fi

if [ "$ENABLEIDISK" = "YES" ]; then
#Uploads encrypted archive to your iDisk
echo "Copying archive to iDisk "
growlnotify -m "Copying to iDisk" -t "Sauvegarde" -P $GROWLPASS --udp --host $GROWLHOST
cp $NAMEE /Volumes/iDisk
fi

#removes archive
#echo "removing archives"
growlnotify -m "Effacement des fichiers temporaires..." -t "Sauvegarde" -P $GROWLPASS --udp --host $GROWLHOST
rm $NAMET
if [ "$REMOVETEMPE" = "YES" ]; then
rm $NAMEE
fi

growlnotify -m "La sauvegarde de $BACKUPNAME est complétée." -t "Sauvegarde" -s -P $GROWLPASS --udp --host $GROWLHOST
