#!/bin/bash

echo "-=-=-=-=-=-=- Git and npm settings -=-=-=-=-=-=-"
echo "Author name?"
read name
echo "Email ?"
read email

git config --global user.useConfigOnly true
git config --global user.gitlab.name "$name"
git config --global user.gitlab.email $email
git config --global user.github.name "$name"
git config --global user.github.email $email

echo "Add signingkey to git identities:"
echo "$ gpg2 --full-gen-key"
echo "$ gpg2 --list-secret-keys --keyid-format LONG"
echo "$ gpg2 --armor --export [serial]"
echo "Copy public key to Gitlab/GitHub user settings."
read

# Set alias for identities

git config --global alias.identity '! git config user.name "$(git config user.$1.name)"; git config user.email "$(git config user.$1.email)"; git config user.signingkey "$(git config user.$1.signingkey)"; :'

if [ ! -f ~/.ssh/id_rsa.pub ]; then
    ssh-keygen
fi

alias pubkey="more ~/.ssh/id_rsa.pub | pbcopy | echo '=> Public key copied to pasteboard.'"
