#!/bin/bash

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.1/install.sh | bash

# Default Packages settings

npm set init.author.name "$name"
npm set init.author.email "$email"
npm set init.license "MIT"
npm set init.version "0.1.0"

# Tools

npm i -g jrlo
npm i -g tldr

# Dev

npm i -g jest
npm i -g typescript
npm i -g @angular/cli
npm i -g @nestjs/cli
npm i -g eslint

# Processes

npm i -g nodemon
npm i -g ts-node

# Rebase editor

 npm i -g rebase-editor
 git config --global sequence.editor rebase-editor
