#!/bin/bash

brew install \
    bitwarden-cli \
    gnupg \
    ffmpeg \
    zsh \
    zsh-completions \
    wget

# brew install git-lfs

brew cask install \
    google-chrome \
    firefox \
    bitwarden \
    sublime-text \
    postman \
    iterm2 \
    visual-studio-code

# brew cask install java # Install OpenJDK

# INSTALLATIONS FACULTATIVES, decommenter ceux nécessaires
# brew cask install slack
# brew cask install sourcetree
# brew cask install docker
# brew cask install robo-3t
# brew cask install spotify
# brew cask install vlc
# brew cask install figma
# brew cask install steam
# brew install hub # https://hub.github.com/

// Autres fonts: https://github.com/Homebrew/homebrew-cask-fonts
brew tap homebrew/cask-fonts
brew cask install font-hack-nerd-font
brew cask install font-fira-code
brew cask install font-jetbrains-mono
brew cask install font-victor-mono
