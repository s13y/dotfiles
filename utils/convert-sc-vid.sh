#!/bin/bash

# Converts sreen capture video to a lightweight video
# Steps:
# - Decrease video size by half
# - Drop frames to speed up video
# - Convert to mp4

# Step 1

# ffmpeg -i $1 -vf "scale=trunc(iw/4)*2:trunc(ih/4)*2" $1_r1.mov

# # Step 2

# ffmpeg -i $1_r1.mov -filter:v "setpts=0.5*PTS"  $1_r2.mov

# # Step 3

# ffmpeg -i $1_r2.mov -qscale 0 $1_r2.mp4


# rm $1_r1.mov
# rm 

$filename="${1##*/}"

echo $filename
