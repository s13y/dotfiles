# Git Setup

Requirements:

* Git is installed
* npm is available

## Configuration

    $ git config --global user.email "you@example.com"
    $ git config --global user.name "Your Name"
    $ git config --global pull.ff only
    $ git config --global core.editor nano
    $ npm i -g rebase-editor
    $ git config --global sequence.editor rebase-editor

Identities: voir wiki gestion_configuration:git:toc#identities
