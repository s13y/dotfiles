#!/bin/bash

cd ~

sudo update-locale LANG=en_US.UTF-8

function base_install {
  echo --------------------------------
  echo Base install...
  echo --------------------------------

  sudo apt-get update

  sudo apt-get install curl -y

  return
}

function configure_ssh {
  echo --------------------------------
  echo Création de la clé ssh
  echo --------------------------------

  ssh-keygen -t ed25519 -a 100

  echo --------------------------------
  echo OpenSSH Server requis ?
  echo --------------------------------

  sudo apt-get install openssh-server -y
  service ssh status

  return
}

function configure_git {
  echo --------------------------------
  echo Installation Git
  echo --------------------------------

  sudo apt-get install git -y

  echo "Git enter user.name :"
  read un
  echo "Git enter user.email"
  read ue

  git config --global user.name "$un"
  git config --global user.email "$ue"
  git config --global core.editor nano

  return
}

function configure_desktop {
  sudo apt-get install snapd -y
  sudo apt-get install unrar -y
  sudo apt install font-manager -y

  return
}

function install_chrome {
  echo --------------------------------
  echo Install Chrome
  echo --------------------------------

  cd ~/Downloads
  wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
  sudo apt install ./google-chrome-stable_current_amd64.deb -y

  cd ~
  return
}

function install_zsh {
  echo ''
  echo "Now installing zsh..."
  echo ''
  sudo apt install zsh -y

  echo ''
  echo "Now installing git and bash-completion..."
  sudo apt-get install git bash-completion -y

  echo ''
  echo "Now configuring git-completion..."
  GIT_VERSION=`git --version | awk '{print $3}'`
  URL="https://raw.github.com/git/git/v$GIT_VERSION/contrib/completion/git-completion.bash"
  echo ''
  echo "Downloading git-completion for git version: $GIT_VERSION..."
  if ! curl "$URL" --silent --output "$HOME/.git-completion.bash"; then
	  echo "ERROR: Couldn't download completion script. Make sure you have a working internet connection." && exit 1
  fi

  # oh-my-zsh install
  echo ''
  echo "Now installing oh-my-zsh..."
  echo ''
  sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

  # oh-my-zsh plugin install
  echo ''
  echo "Now installing oh-my-zsh plugins..."
  echo ''

  git clone https://github.com/zsh-users/zsh-completions ~/.oh-my-zsh/custom/plugins/zsh-completions
  git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
  git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

  autoload -U compinit && compinit

  # powerlevel9k install
  echo ''
  echo "Now installing powerlevel9k..."
  echo ''
  git clone https://github.com/bhilburn/powerlevel9k.git ~/.oh-my-zsh/custom/themes/powerlevel9k

  wget https://gitlab.com/s13y/dotfiles/raw/master/zsh/.zshrc.linux

  mv ~/.zshrc ~/.zshrc.bak
  mv ~/.zshrc.linux ~/.zshrc
  source .zshrc

  echo "=========================================="
  echo "Ajouter aux plugins de zshrc:"
  echo "plugins=(git zsh-autosuggestions zsh-completions zsh-syntax-highlighting)"
  echo "=========================================="

  return
}

base_install

echo ''
echo "Install and setup SSH?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) configure_ssh; break;;
        No ) break;;
    esac
done

echo ''
echo "Install and setup Git?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) configure_git; break;;
        No ) break;;
    esac
done

echo ''
echo "Install zsh with oh-my-zsh?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) install_zsh; break;;
        No ) break;;
    esac
done

echo ''
echo "Desktop or server?"
select yn in "Desktop" "Server"; do
    case $yn in
        Desktop ) configure_desktop; break;;
        Server ) configure_server; break;;
    esac
done

echo ''
echo 'All done.'
echo ''

exit
