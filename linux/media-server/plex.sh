#!/bin/bash

echo "Go to https://www.plex.tv/media-server-downloads/#plex-media-server and copy complete URL of deb file"
echo "ex: https://downloads.plex.tv/plex-media-server-new/1.18.7.2457-77cb9455c/debian/plexmediaserver_1.18.7.2457-77cb9455c_amd64.deb"
read plexdeburl

cd ~/Downloads
wget $plexdeburl
sudo dpkg -i plexmediaserver_*.deb

sudo systemctl status plexmediaserver
dpkg -L plexmediaserver

echo "====[ Adding Plex Source ]=============================================="
sudo nano /etc/apt/sources.list.d/plexmediaserver.list
wget -q https://downloads.plex.tv/plex-keys/PlexSign.key -O - | sudo apt-key add -
sudo apt update

export LD_LIBRARY_PATH=/usr/lib/plexmediaserver

# https://support.plex.tv/hc/en-us/articles/201242707-Plex-Media-Scanner-via-Command-Line

alias pms="/usr/lib/plexmediaserver/Plex\ Media\ Scanner"
