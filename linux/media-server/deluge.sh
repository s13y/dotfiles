#!/bin/bash

sudo add-apt-repository ppa:deluge-team/ppa -y
sudo apt install deluged deluge-console deluge-webui -y

sudo groupadd deluge
sudo adduser --system --gecos "Deluge Service" --disabled-password --group --home /var/lib/deluge deluge
sudo chown -R deluge:deluge /var/lib/deluge

sudo gpasswd -a $(whoami) deluge

# Run deluged to create config files
sudo setsid sh -c 'exec deluged <> /dev/tty2 >&0 2>&1'
sudo pkill deluged

echo ""
echo "====[ ~/.config/deluge/auth modification ]=============================="
echo "User name for deluged:"
read user
echo "Password for deluged:"
read password
echo "$user:$password:10" >> ~/.config/deluge/auth

echo ""
echo "====[ deluge-console config ]==========================================="
echo "In deluge-console"
echo "  1. Enter 'config -s allow_remote True'"
echo "  2. 'quit'"
read -p "Press [Enter] to continue..."
sudo setsid sh -c 'exec deluged <> /dev/tty2 >&0 2>&1'

read -p "Press [Enter] to continue..."

deluge-console
sudo pkill deluged

echo ""
echo "====[ Copy deluged{-web}.service ]============================================"
sudo cp ./deluged.service /etc/systemd/system
sudo cp ./deluge-web.service /etc/systemd/system

sudo systemctl daemon-reload

sudo systemctl start deluged
sudo systemctl enable deluged

sudo systemctl start deluge-web
sudo systemctl enable deluge-web
