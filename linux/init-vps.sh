#!/bin/bash

cd ~

echo --------------------------------
echo Installation Git, Curl
echo --------------------------------

sudo apt-get update

sudo apt-get install curl -y
sudo apt-get install git -y

echo --------------------------------
echo Clone dotfiles
echo --------------------------------

git clone https://gitlab.com/s13y/dotfiles.git

echo --------------------------------
echo Install ZSH
echo --------------------------------

sh ./dotfiles/zsh/install-zsh.sh

mv ~/.zshrc ~/.zshrc.bak
cp ./dotfiles/zsh/.zshrc.linux ~/.zshrc
source .zshrc

echo --------------------------------
echo Next steps:
echo * change SSH port
