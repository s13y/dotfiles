# Linux Server Setup

* init-server.sh

## Systemd Journal

To limit size of journalization, we can edit the configuration file:

    sudo nano /etc/systemd/journald.conf 

Set the `SystemMaxUse`, for example, for 100M:

    SystemMaxUse=100M

