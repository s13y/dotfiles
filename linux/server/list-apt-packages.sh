#!/bin/bash

# List all packages intentionally installed via apt

(zcat $(ls -tr /var/log/apt/history.log*.gz); cat /var/log/apt/history.log) 2>/dev/null |
  egrep '^(Commandline:)' |
  egrep '(install|remove)' |
  grep -v 'autoremove'

