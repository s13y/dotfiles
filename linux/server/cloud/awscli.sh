#!/bin/bash

python --version
python3 --version

read -p "Python doit être installé avec les versions 2.6.5+ ou 3.3+.  Enter pour continuer, Ctrl+C pour arrêter."

curl -O https://bootstrap.pypa.io/get-pip.py
python get-pip.py --user

read -p "Prend en compte un shell Zsh..."

echo "export PATH=~/.local/bin:\$PATH" >> ~/.zshrc

source ~/.zshrc

pip --version

pip install awscli --upgrade --user

aws --version
