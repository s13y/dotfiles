#!/bin/bash

sudo apt-get update

echo STATSD

sudo apt-get install git nodejs devscripts debhelper -y


echo GRAPHITE

sudo apt-get install graphite-web graphite-carbon -y

sudo apt-get install postgresql libpq-dev python-psycopg2 -y

sudo apt-get install apache2 libapache2-mod-wsgi -y

