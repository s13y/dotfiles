#!/bin/bash

echo --------------------------------
echo Création de la clé ssh
echo --------------------------------

ssh-keygen -t ed25519 -a 100

echo --------------------------------
echo OpenSSH Server requis ?
echo --------------------------------

sudo apt-get install openssh-server -y
service ssh status

echo --------------------------------
echo Installation Git, Curl
echo --------------------------------

sudo apt-get update

sudo apt-get install curl -y
sudo apt-get install git -y


echo "Git enter user.name :"
read un
echo "Git enter user.email"
read ue

git config --global user.name "$un"
git config --global user.email "$ue"
git config --global core.editor nano
