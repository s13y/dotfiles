#!/bin/bash

sudo update-locale LANG=en_US.UTF-8

sudo apt-get install snapd -y
sudo apt-get install unrar -y
sudo apt install font-manager -y

echo --------------------------------
echo Install Chrome
echo --------------------------------

cd ~/Downloads
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo apt install ./google-chrome-stable_current_amd64.deb -y

sudo snap install bitwarden
sudo snap install bw
sudo snap install spotify
sudo snap install --classic code
sudo snap install vlc
sudo snap install discord
sudo snap install postman
